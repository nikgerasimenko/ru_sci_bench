from collections import defaultdict

import os
import numpy as np
import pandas as pd
import json
from lightning.classification import LinearSVC
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import NearestNeighbors

from paths import DataPaths
from embeddings import load_embeddings_from_jsonl

np.random.seed(1)


def get_ru_sci_bench_metrics(
    embeddings_path,
    metrics="all",
    get_cls_report=False,
    grid_search_cv=False,
    n_jobs=-1,
    max_iter=100
):
    data_paths = DataPaths(f'{os.path.dirname(__file__)}/data')
    print("Loading embeddings...")
    embeddings = load_embeddings_from_jsonl(embeddings_path)

    results = {}

    if metrics in ["all", "translation_search"] or "translation_search" in metrics:
        print("Running the eLibrary translation search task...")
        with open(data_paths.ru_en_translation_test) as f:
            ru_en_translation_test = json.load(f)

        ru_ids = [int(id_) for id_ in ru_en_translation_test.keys()]
        en_ids = [int(id_) for id_ in ru_en_translation_test.values()]
        results["ru_en_translation_search"] = translation_recall_at_1(
            embeddings,
            ru_en_translation_test,
            queries_ids=ru_ids,
            results_ids=en_ids,
            n_jobs=n_jobs
        )
        print("ru_en_translation_search_recall@1", results["ru_en_translation_search"])

        results["en_ru_translation_search"] = translation_recall_at_1(
            embeddings,
            ru_en_translation_test,
            queries_ids=en_ids,
            results_ids=ru_ids,
            n_jobs=n_jobs
        )
        print("en_ru_translation_search_recall@1", results["en_ru_translation_search"])

    if metrics in ["all", "full"] or "full" in metrics:
        print("Running the eLibrary OECD-full task...")
        X, y = get_X_y_for_classification(
            embeddings,
            data_paths.elibrary_oecd_full_train,
            data_paths.elibrary_oecd_full_test,
        )
        results["elibrary_oecd_full"] = classify(
            X["train"], y["train"], X["test"], y["test"], get_cls_report=get_cls_report,
            grid_search_cv=grid_search_cv, n_jobs=n_jobs, max_iter=max_iter
        )
        print("elibrary_oecd_full_macro_f1", results["elibrary_oecd_full"]["macro_f1"])

        print("Running the eLibrary GRNTI-full task...")
        X, y = get_X_y_for_classification(
            embeddings,
            data_paths.elibrary_grnti_full_train,
            data_paths.elibrary_grnti_full_test,
        )
        results["elibrary_grnti_full"] = classify(
            X["train"], y["train"], X["test"], y["test"], get_cls_report=get_cls_report,
            grid_search_cv=grid_search_cv, n_jobs=n_jobs, max_iter=max_iter
        )
        print("elibrary_grnti_full_macro_f1", results["elibrary_grnti_full"]["macro_f1"])

    if metrics in ["all", "ru"] or "ru" in metrics:
        print("Running the eLibrary OECD-ru task...")
        X, y = get_X_y_for_classification(
            embeddings,
            data_paths.elibrary_oecd_ru_train,
            data_paths.elibrary_oecd_ru_test,
        )
        print("Classifier training...")
        results["elibrary_oecd_ru"] = classify(
            X["train"], y["train"], X["test"], y["test"], get_cls_report=get_cls_report,
            grid_search_cv=grid_search_cv, n_jobs=n_jobs, max_iter=max_iter
        )
        print("elibrary_oecd_ru_macro_f1", results["elibrary_oecd_ru"]["macro_f1"])

        print("Running the eLibrary GRNTI-ru task...")
        X, y = get_X_y_for_classification(
            embeddings,
            data_paths.elibrary_grnti_ru_train,
            data_paths.elibrary_grnti_ru_test,
        )
        print("Classifier training...")
        results["elibrary_grnti_ru"] = classify(
            X["train"], y["train"], X["test"], y["test"], get_cls_report=get_cls_report,
            grid_search_cv=grid_search_cv, n_jobs=n_jobs, max_iter=max_iter
        )
        print("elibrary_grnti_ru_macro_f1", results["elibrary_grnti_ru"]["macro_f1"])

    if metrics in ["all", "en"] or "en" in metrics:
        print("Running the eLibrary OECD-en task...")
        X, y = get_X_y_for_classification(
            embeddings,
            data_paths.elibrary_oecd_en_train,
            data_paths.elibrary_oecd_en_test,
        )
        print("Classifier training...")
        results["elibrary_oecd_en"] = classify(
            X["train"], y["train"], X["test"], y["test"], get_cls_report=get_cls_report,
            grid_search_cv=grid_search_cv, n_jobs=n_jobs, max_iter=max_iter
        )
        print("elibrary_oecd_en_macro_f1", results["elibrary_oecd_en"]["macro_f1"])

        print("Running the eLibrary GRNTI-en task...")
        X, y = get_X_y_for_classification(
            embeddings,
            data_paths.elibrary_grnti_en_train,
            data_paths.elibrary_grnti_en_test,
        )
        print("Classifier training...")
        results["elibrary_grnti_en"] = classify(
            X["train"], y["train"], X["test"], y["test"], get_cls_report=get_cls_report,
            grid_search_cv=grid_search_cv, n_jobs=n_jobs, max_iter=max_iter
        )
        print("elibrary_grnti_en_macro_f1", results["elibrary_grnti_en"]["macro_f1"])

    return results


def classify(
    X_train,
    y_train,
    X_test,
    y_test,
    get_cls_report=False,
    grid_search_cv=False,
    n_jobs=1,
    max_iter=100
):
    """
    Simple classification methods using sklearn framework.
    Selection of C happens inside of X_train, y_train via
    cross-validation.

    Arguments:
        X_train, y_train -- training data
        X_test, y_test -- test data to evaluate on (can also be validation data)

    Returns:
        F1 on X_test, y_test (out of 100), rounded to two decimal places
    """
    estimator = LinearSVC(loss="squared_hinge", max_iter=max_iter, random_state=42)
    if grid_search_cv:
        Cs = np.logspace(-4, 2, 7)
        svm = GridSearchCV(
            estimator=estimator,
            cv=3,
            param_grid={"C": Cs},
            verbose=1,
            n_jobs=n_jobs
        )
    else:
        svm = estimator
    svm.fit(X_train, y_train)
    y_pred = svm.predict(X_test)
    result = {
        "macro_f1": f1_score(y_test, y_pred, average="macro"),
        "weighted_f1": f1_score(y_test, y_pred, average="weighted")
    }
    if get_cls_report:
        result["cls_report"] = classification_report(y_test, y_pred)
    return result


def get_X_y_for_classification(embeddings, train_path, test_path):
    """
    Given the directory with train/test/val files for mesh classification
        and embeddings, return data as X, y pair

    Arguments:
        embeddings: embedding dict
        mesh_dir: directory where the mesh ids/labels are stored
        dim: dimensionality of embeddings

    Returns:
        X, y: dictionaries of training X and training y
              with keys: 'train', 'val', 'test'
    """
    dim = len(next(iter(embeddings.values())))
    train = pd.read_csv(train_path)
    test = pd.read_csv(test_path)

    X = defaultdict(list)
    y = defaultdict(list)
    for dataset_name, dataset in zip(["train", "test"], [train, test]):
        for s2id, class_label in dataset.values:
            X[dataset_name].append(embeddings[s2id])
            y[dataset_name].append(class_label)
        X[dataset_name] = np.array(X[dataset_name])
        y[dataset_name] = np.array(y[dataset_name])
    return X, y


def translation_recall_at_1(
    embeddings: np.array,
    ru_en_translation_test: np.array,
    queries_ids: list,
    results_ids: list,
    n_jobs=1
):
    queries_embs = np.concatenate([[embeddings[id_]] for id_ in queries_ids])
    results_embs = np.concatenate([[embeddings[id_]] for id_ in results_ids])
    # Create a NearestNeighbors model using cosine distance
    nbrs = NearestNeighbors(n_neighbors=1, metric="cosine", n_jobs=n_jobs)

    # Fit the model with the English vectors
    nbrs.fit(results_embs)

    # Find the top nearest neighbor for each Russian vector
    _, top_100_indices = nbrs.kneighbors(queries_embs)

    # Check if the correct translation is in the top 1 similar translations
    correct_indices = np.arange(results_embs.shape[0])
    correct_translations_in_top_k = (
        np.any(top_100_indices == correct_indices[:, np.newaxis], axis=1)
    ).astype(int)
    return {"recall@1": len(correct_translations_in_top_k.nonzero()[0]) / len(correct_translations_in_top_k)}
