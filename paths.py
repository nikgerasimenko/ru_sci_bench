import os


PROJECT_ROOT_PATH = os.path.abspath(os.path.join(os.getcwd()))
    
    
class DataPaths:
    def __init__(self, base_path=None):
        if base_path is None:
            base_path = os.path.join(PROJECT_ROOT_PATH, 'data')
        self.base_path = base_path

        self.elibrary_oecd_full_train = os.path.join(base_path, 'elibrary_oecd_full', 'train.csv')
        self.elibrary_oecd_full_test = os.path.join(base_path, 'elibrary_oecd_full', 'test.csv')

        self.elibrary_grnti_full_train = os.path.join(base_path, 'elibrary_grnti_full', 'train.csv')
        self.elibrary_grnti_full_test = os.path.join(base_path, 'elibrary_grnti_full', 'test.csv')

        self.elibrary_oecd_ru_train = os.path.join(base_path, 'elibrary_oecd_ru', 'train.csv')
        self.elibrary_oecd_ru_test = os.path.join(base_path, 'elibrary_oecd_ru', 'test.csv')

        self.elibrary_grnti_ru_train = os.path.join(base_path, 'elibrary_grnti_ru', 'train.csv')
        self.elibrary_grnti_ru_test = os.path.join(base_path, 'elibrary_grnti_ru', 'test.csv')

        self.elibrary_oecd_en_train = os.path.join(base_path, 'elibrary_oecd_en', 'train.csv')
        self.elibrary_oecd_en_test = os.path.join(base_path, 'elibrary_oecd_en', 'test.csv')

        self.elibrary_grnti_en_train = os.path.join(base_path, 'elibrary_grnti_en', 'train.csv')
        self.elibrary_grnti_en_test = os.path.join(base_path, 'elibrary_grnti_en', 'test.csv')

        self.ru_en_translation_test = os.path.join(base_path, 'ru_en_translation_test.json')
